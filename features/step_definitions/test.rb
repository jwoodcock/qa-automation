When(/^I visit google.co.uk$/) do
  visit('https://www.google.co.uk')
end

Then(/^I can see the google logo$/) do
  expect(page).to have_css('[id="hplogo"]')
end
