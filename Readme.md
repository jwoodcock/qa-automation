# BBQA-automation-2.0 aka Steve

Extremely quick guide for creating a test project on a Debian machine with Vagrant, and run your first cucumber test using latest Ruby version and Chrome Headless.

## Prerequisites

- Vagrant (https://www.vagrantup.com/)
- VirtualBox (https://www.virtualbox.org/wiki/Downloads)

## Install

#### Create an empty directory called 'vagrant', initialize the debian box, install it, and ssh into it

```bash
mkdir vagrant
cd vagrant
vagrant init debian/jessie64
vagrant up
vagrant ssh
```

#### You're now in the vagrant debian box! Woho! Great Work!


#### Install RVM and latest ruby version

```bash
sudo apt-get update
sudo apt-get install curl
gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable
source /home/vagrant/.rvm/scripts/rvm
rvm reload
rvm install 2.4.1
rvm use 2.4.1
```

#### Install your favourite text editor (I'm using Vim in this example)

```bash 
sudo apt-get install vim
```

#### Install bundler

```bash
gem install bundler
```

#### Create a new automation directory and grant yourself root permissions

```bash
sudo mkdir /home/automation
cd /home/automation
sudo chown -R $(whoami):$(whoami) /home/automation/
```

#### Create Gemfile with the text editor of choice

```bash
vim Gemfile
```

```ruby
source "https://www.rubygems.org"

gem 'cucumber'
gem 'capybara'
gem 'rspec'
gem 'chromedriver-helper'
gem 'selenium-webdriver'
```

#### Install the gems

```bash
bundle install
```

#### Install latest Google-chrome version

```bash
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
sudo apt-get update
sudo apt-get install google-chrome-stable
```

#### Initialize cucumber

```bash
cucumber --init
```

#### Update env.rb

```bash
sudo vim features/support/env.rb
```
 
```ruby
require 'capybara/cucumber'
require 'selenium-webdriver'

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new app, browser: :chrome,
  options: Selenium::WebDriver::Chrome::Options.new(args: %w[headless disable-gpu])
end

Capybara.default_driver = :chrome
Capybara.default_max_wait_time = 10

After do |scenario, feature|
  Capybara.current_session.driver.quit
end
```


#### Create your first test

```bash
sudo vim features/first_test.feature
```

```gherkin
Feature: First Test

  Scenario: I can see the google logo
    When I visit google.co.uk
    Then I can see the google logo 
```

```bash
sudo vim features/step_definitions/first_test.rb
```

```ruby
When(/^I visit google.co.uk$/) do
  visit('https://www.google.co.uk')
end

Then(/^I can see the google logo$/) do
  expect(page).to have_css('[id="hplogo"]')
end
```


## Usage

```bash
cucumber
```

## Contribute

PRs accepted.

## License

MIT © Marius Jørgensen

